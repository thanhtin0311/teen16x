import { Component, OnInit } from '@angular/core';
import { MovieService } from '../../core/movie.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  public tags: Array<any> = [];

  constructor(private movieService: MovieService,
    private router: Router) { }

  ngOnInit(): void {
    this.listTagMovie();
  }

  async listTagMovie() {
    const res: any = await this.movieService.readAllTag();
    const resData = res.data;
    if (resData) {
      const shuffled = [...resData].sort(() => 0.5 - Math.random());
      this.tags = shuffled.slice(0, 23);
    }
  }
  detailTag(code: string) {
    this.router.navigate(['tag', code]);
  }
}
