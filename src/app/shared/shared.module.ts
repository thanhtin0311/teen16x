import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent, HeaderComponent } from './';
import { ViewPipe } from './pipes/view.pipe';
import { MovieListComponent } from './movie-list/movie-list.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [FooterComponent, HeaderComponent, MovieListComponent, ViewPipe],
  imports: [CommonModule, FormsModule],
  exports: [FooterComponent, HeaderComponent, MovieListComponent],
})
export class SharedModule { }
