import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { MovieService } from '../../core/movie.service';
import { MOVIE_ORDERING, PADDING_DEFAULT } from '../helper/constant';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {
  public isShow = false;
  public domain: any = {};
  public orders: Array<any> = [];
  public order: any = {};
  public movies: Array<any> = [];
  public limit: number = PADDING_DEFAULT.LIMIT_DEFAULT;
  public offset: number = PADDING_DEFAULT.OFFSET_DEFAULT;
  public pageSize: number = PADDING_DEFAULT.LIMIT_DEFAULT;
  public pages: Array<any> = [];
  public page: any = {};
  public totalCount: number = 0;
  @Input() category: any = {};
  @Input() tag: any = {};
  @Input() title: string = '';
  @Input() keyword: string = '';
  @Input() pornstarCode: string = '';
  public defaultImage = "/assets/images/default.jpg";
  constructor(private movieService: MovieService,
    private router: Router) { }

  ngOnInit(): void {
    this.initData();
  }
  async initData() {
    this.domain = await this.getDomainInfo();
    this.setOrder();
    this.initVideo();

  }
  async initVideo() {
    await this.listMovie();
    this.setPage(this.totalCount);

  }
  ngOnChanges(changes: SimpleChanges) {
    if (changes['category']) {
      this.initData();
    }
    if (changes['tag']) {
      this.initData();
    }
    if (changes['keyword']) {
      this.initData();
    }
  }
  async listMovie() {
    const res: any = await this.movieService.listMovie({
      limit: this.limit || PADDING_DEFAULT.OFFSET_DEFAULT,
      offset: this.offset || PADDING_DEFAULT.OFFSET_DEFAULT,
      order: this.order && this.order.value ? this.order.value : MOVIE_ORDERING.NEWEST,
      categoryId: this.category.id,
      tagCode: this.tag.code,
      filterString: this.keyword,
      authorCode: this.pornstarCode,

    });
    const resData = res.data;
    if (resData && resData.rows) {
      this.movies = resData.rows;
      this.totalCount = resData.count;
    }
  }

  async setPage(totalCount: number) {
    this.pages = [];
    let pageIndex = 1;
    for (let index = 0; index < totalCount; index += this.pageSize) {
      this.pages.push({
        pageIndex: pageIndex,
        isActive: pageIndex == 1 ? true : false
      })
      pageIndex++;
    }
    this.page = this.pages && this.pages.length > 0 ? this.pages[0] : {};
  }

  async setOrder() {
    this.orders = this.movieService.orders;
    this.order = this.orders[0];
  }
  async getDomainInfo() {
    let result;
    const res: any = await this.movieService.getDomainInfo();
    if (res && res.data) {
      result = res.data;
    }
    return result;
  }

  async changeOrder(data: any) {
    this.orders = this.orders.map(order => {
      return {
        ...order,
        isActive: false
      }
    });
    this.orders[data.index].isActive = true;
    this.order = this.orders[data.index];
    await this._resetData();
    this.listMovie();
  }

  _resetData() {
    this.setPage(this.totalCount);
    this.limit = PADDING_DEFAULT.LIMIT_DEFAULT;
    this.offset = PADDING_DEFAULT.OFFSET_DEFAULT;
  }
  changePage(data: any) {
    const pageIndex = data.pageIndex;
    this.page = data;
    this._resetActivePage();
    this.pages[pageIndex - 1].isActive = true;
    this.offset = this.pageSize * (pageIndex - 1);
    this.listMovie();
  }
  previous() {
    const newCurrentIndex = this.page.pageIndex - 1;
    this.page = this.pages[newCurrentIndex - 1];
    this._resetActivePage();
    this.pages[newCurrentIndex - 1].isActive = true;
    this.offset = this.pageSize * (newCurrentIndex - 1);
    this.listMovie();
  }
  next() {
    const newCurrentIndex = this.page.pageIndex + 1;
    this.page = this.pages[newCurrentIndex - 1];
    this._resetActivePage();
    this.pages[newCurrentIndex - 1].isActive = true;
    this.offset = this.pageSize * (newCurrentIndex - 1);
    this.listMovie();
  }
  _resetActivePage() {
    this.pages = this.pages.map(page => {
      return {
        ...page,
        isActive: false
      }
    });
  }
  detailVideo(code: string) {
    this.router.navigate(['', code]);
  }

  toggleFilters() {
    this.isShow = !this.isShow;
  }
}
