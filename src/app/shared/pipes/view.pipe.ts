import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: 'view' })
export class ViewPipe implements PipeTransform {
  transform(value: number): string {
    return `${(Math.round(value / 100) / 10).toString()}k`;
  }
}
