import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public keyword: string = '';
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  async onChangeKeyword() {
    console.log('1233');
    if (this.keyword && this.keyword.length > 0) {
      console.log(this.keyword);
      this.router.navigate(['/search', this.keyword])
    } else {
      this.router.navigate(['/search', ''])
    }
  }

}
