import { Component, OnInit } from '@angular/core';
import { MovieService } from '../../core/movie.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  categories: any = {};
  constructor(private movieService: MovieService, private router: Router) { }

  ngOnInit(): void {
    this.getDomainInfo();
  }

  async getDomainInfo() {
    const res: any = await this.movieService.getDomainInfo();
    let result;
    if (res && res.data && res.data.categories) {
      result = res.data.categories;
      const shuffled = [...result].sort(() => 0.5 - Math.random());
      this.categories = shuffled.slice(0, 15);
    }
  }

  detailCategory(code: string) {
    this.router.navigate(['categories', code]);
  }

}
