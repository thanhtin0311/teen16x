import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  public categories: Array<any> = [];

  constructor(private route: ActivatedRoute,
    private router: Router) { }

  keyword: any;
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const keyword = params.get('keyword');
      console.log('keyword', this.keyword);
      this.keyword = keyword;
    });
  }

  detailCategory(code: string) {
    this.router.navigate(['categories', code]);
  }
}
