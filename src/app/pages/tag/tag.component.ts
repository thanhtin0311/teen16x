import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MovieService } from 'src/app/core/movie.service';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.css']
})
export class TagComponent implements OnInit {
  public categories: Array<any> = [];
  public tag: any = {};
  constructor(
    private route: ActivatedRoute,
    private movieService: MovieService,
    private router: Router
  ) {
  }
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const tagCode = params.get('code');
      this.initData(tagCode);
    });
    this.getDomainInfo();
  }
  async initData(tagCode: any) {
    const resTag: any = await this.movieService.readTagByCode(tagCode);
    this.tag = resTag.data;
  }

  async getDomainInfo() {
    const res: any = await this.movieService.getDomainInfo();
    let result;
    if (res && res.data && res.data.categories) {
      result = res.data.categories;
      const shuffled = [...result].sort(() => 0.5 - Math.random());
      this.categories = shuffled.slice(0, 15);
    }
  }
  detailCategory(code: string) {
    this.router.navigate(['categories', code]);
  }
}
