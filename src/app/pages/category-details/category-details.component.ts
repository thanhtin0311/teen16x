import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MovieService } from 'src/app/core/movie.service';

@Component({
  selector: 'app-category-details',
  templateUrl: './category-details.component.html',
  styleUrls: ['./category-details.component.css']
})
export class CategoryDetailsComponent implements OnInit {
  public category: any = {};
  public tags: Array<any> = [];

  constructor(
    private route: ActivatedRoute,
    private movieService: MovieService,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const categoryCode = params.get('code');
      this.initData(categoryCode);
    });
    this.listTagMovie();
  }

  async initData(categoryCode: any) {
    const resCategory: any = await this.movieService.readCategoryByCode(categoryCode);
    this.category = resCategory.data;
  }

  async listTagMovie() {
    const res: any = await this.movieService.readAllTag();
    const resData = res.data;
    if (resData) {
      const shuffled = [...resData].sort(() => 0.5 - Math.random());
      this.tags = shuffled.slice(0, 13);
    }
  }

  detailTag(code: string) {
    this.router.navigate(['tag', code]);
  }
}
