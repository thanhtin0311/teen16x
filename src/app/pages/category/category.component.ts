import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MovieService } from '../../core/movie.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  public categories: Array<any> = [];
  public tags: Array<any> = [];

  constructor(private movieService: MovieService,
    private router: Router) { }

  ngOnInit(): void {
    this.categoryList();
    this.listTagMovie();
  }

  async categoryList() {
    const res: any = await this.movieService.getDomainInfo();
    const data = res.data;
    if (data && data.categories) {
      this.categories = data.categories;
    }
  }

  async listTagMovie() {
    const res: any = await this.movieService.readAllTag();
    const resData = res.data;
    if (resData) {
      const shuffled = [...resData].sort(() => 0.5 - Math.random());
      this.tags = shuffled.slice(0, 13);
    }
  }
  detailCategory(code: string) {
    this.router.navigate(['categories', code]);
  }

  detailTag(code: string) {
    this.router.navigate(['tag', code]);
  }
}
