import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import {
  HomeComponent,
  NewestComponent,
  CategoryComponent,
  PornstarComponent,
  CategoryDetailsComponent,
} from './';
import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { PornstarDetailsComponent } from './pornstar-details/pornstar-details.component';
import { SearchComponent } from './search/search.component';
import { TagComponent } from './tag/tag.component';

@NgModule({
  declarations: [
    HomeComponent,
    NewestComponent,
    CategoryComponent,
    PornstarComponent,
    CategoryDetailsComponent,
    MovieDetailsComponent,
    PornstarDetailsComponent,
    SearchComponent,
    TagComponent,
  ],
  imports: [CommonModule, SharedModule],
})
export class PagesModule { }
