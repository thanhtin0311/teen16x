import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MovieService } from 'src/app/core/movie.service';

@Component({
  selector: 'app-pornstar-details',
  templateUrl: './pornstar-details.component.html',
  styleUrls: ['./pornstar-details.component.css']
})
export class PornstarDetailsComponent implements OnInit {
  public categories: Array<any> = [];
  public pornstar: any = {};
  public pornstarCode: any = '';
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private movieService: MovieService
  ) {
  }
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.pornstarCode = params.get('code');
      this.initData(this.pornstarCode);
    });
    this.getDomainInfo();
  }
  async initData(pornstarCode: any) {
    const resPornstar: any = await this.movieService.readPornstar(pornstarCode);
    this.pornstar = resPornstar.data;
  }

  async getDomainInfo() {
    const res: any = await this.movieService.getDomainInfo();
    let result;
    if (res && res.data && res.data.categories) {
      result = res.data.categories;
      const shuffled = [...result].sort(() => 0.5 - Math.random());
      this.categories = shuffled.slice(0, 15);
    }
  }

  detailCategory(code: string) {
    this.router.navigate(['categories', code]);
  }

}
