import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-newest',
  templateUrl: './newest.component.html',
  styleUrls: ['./newest.component.css']
})
export class NewestComponent implements OnInit {

  constructor(
    private router: Router
  ) {

  }

  ngOnInit(): void {
    this.redirect();
  }

  redirect() {
    this.router.navigate(['']);
  }

}
