import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { MovieService } from 'src/app/core/movie.service';
@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {
  public categories: Array<any> = [];
  public code: any;
  public video: any = {};

  constructor(
    private route: ActivatedRoute,
    private movieService: MovieService,
    private router: Router,
  ) { }
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.code = params.get('code');
      if (this.code == 'search') {
        this.router.navigate(['/']);
      } else {
        this.initData();
      }
    });
  }

  initData() {
    this.setDetailVideo();
    this.getDomainInfo();
  }

  async setDetailVideo() {
    const res: any = await this.movieService.readMovie(this.code);
    const data: any = res.data;
    this.video = data && data.detail ? data.detail : {};
  }

  async getDomainInfo() {
    const res: any = await this.movieService.getDomainInfo();
    let result;
    if (res && res.data && res.data.categories) {
      result = res.data.categories;
      const shuffled = [...result].sort(() => 0.5 - Math.random());
      this.categories = shuffled.slice(0, 15);
    }
  }

  detailCategory(code: string) {
    this.router.navigate(['categories', code]);
  }
}
