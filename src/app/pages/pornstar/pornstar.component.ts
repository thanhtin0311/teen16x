import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MovieService } from 'src/app/core/movie.service';

@Component({
  selector: 'app-pornstar',
  templateUrl: './pornstar.component.html',
  styleUrls: ['./pornstar.component.css']
})
export class PornstarComponent implements OnInit {
  public max = 80;
  public pornstars: Array<any> = [];
  public categories: Array<any> = [];
  constructor(
    private movieService: MovieService,
    private router: Router
  ) {
  }
  ngOnInit() {
    this.pornstarList();
    this.getDomainInfo();
  }

  async pornstarList() {
    const res: any = await this.movieService.getDomainMoreInfo();
    const data = res.data;
    if (data && data.authors) {
      console.log(data.authors);
      this.pornstars = data.authors;
    }
  }
  async getDomainInfo() {
    const res: any = await this.movieService.getDomainInfo();
    let result;
    if (res && res.data && res.data.categories) {
      result = res.data.categories;
      const shuffled = [...result].sort(() => 0.5 - Math.random());
      this.categories = shuffled.slice(0, 15);
    }
  }
  showMore(): void {
    this.max = this.max + 32;
  }
  detailPornstar(code: string) {
    this.router.navigate(['pornstars', code]);
  }

  detailCategory(code: string) {
    this.router.navigate(['categories', code]);
  }

}
