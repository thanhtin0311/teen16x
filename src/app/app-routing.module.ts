import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MovieDetailsComponent } from './pages/movie-details/movie-details.component';
import { NewestComponent } from './pages/newest/newest.component';
import {
  CategoryDetailsComponent,
  HomeComponent,
  CategoryComponent,
  PornstarComponent,
  PornstarDetailsComponent,
  TagComponent,
  SearchComponent,
} from './pages';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'newest', component: NewestComponent },
  { path: 'categories', component: CategoryComponent },
  { path: 'categories/:code', component: CategoryDetailsComponent },
  { path: 'pornstars/:code', component: PornstarDetailsComponent },
  { path: 'pornstars', component: PornstarComponent },
  { path: 'tag/:code', component: TagComponent },
  { path: 'search/:keyword', component: SearchComponent },
  { path: ':code', component: MovieDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
